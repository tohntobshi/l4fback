<?php
include_once 'db-conn.php';

class ImageManager extends DBConn {
    private $conn;
    private $user;
    private $data;
    private $result;
    private $error;
    public function __construct($form) {
        if (!isset($_SESSION['user_id'])) {
            $this->result = null;
            return;
        }
        if (!isset($form['query'])) {
            $this->result = null;
            return;
        }
        $this->user = $_SESSION['user_id'];
        $this->conn = $this->connect();
        $files =  $_FILES;
        switch ($form['query']) {
            case 'upload_images':
                if (!$files) {
                    $this->result = null;
                    return;
                }
                $this->uploadImages($files);
                break;
            case 'change_userpic':
                if (!$files) {
                    $this->result = null;
                    return;
                }
                $this->changeUserPic($files[0]);
                break;
            case 'delete_userpic':
                $this->deleteUserPic();
                break;
        }
    }

    private function uploadImages($files) {
        foreach ($files as $file) {
            if(!$this->checkErrors($file)) {
                $image = $this->saveImage($file);
                if (!$image) {
                    continue;
                }
                $this->data['attachments'][] = $image;
            }
        }
        if(!$this->data['attachments']) {
            return;
        }
        $this->result = true;
    }

    private function checkErrors($file = NULL, $maxSize = 10485760, $allowedExt = array("jpg", "jpeg", "gif", "png")) {
        if(!$file) {
            $this->error[] = "no_file";
            return true;
        }
        if (!isset($file["name"])) {
            $this->error[] = "no_file";
            return true;
        }
        $__fileExt = explode(".", $file["name"]);
        $_fileExt = end($__fileExt);
        $fileExt = strtolower($_fileExt);
        if(!$file["tmp_name"]) {
            $this->error[] = "no_file";
            return true;
        }
        if($file["error"] > 0) {
            $this->error[] = "error_while_uploading";
            return true;
        }
        if(!in_array($fileExt, $allowedExt)) {
            unlink($file["tmp_name"]);
            $this->error[] = "invalid_extention";
            return true;
        }
        if(!in_array(mime_content_type($file["tmp_name"]), ['image/jpeg', 'image/gif', 'image/png'])) {
            unlink($file["tmp_name"]);
            $this->error[] = "invalid_extention";
            return true;
        }
        if($file["size"] > $maxSize) {
            unlink($file["tmp_name"]);
            $this->error[] = "error_invalid_size";
            return true;
        }
            return;
    }
    private function saveImage($file) {
        $fileName = $file["name"];
        $fileTmpLoc = $file["tmp_name"];
        $fileSize = $file["size"];
        list($w, $h) = getimagesize($fileTmpLoc);
        $md5 = hash_file('md5', $fileTmpLoc);
        $sha1 = hash_file('sha1', $fileTmpLoc);
        $query = $this->conn->query("SELECT src, type FROM attachments WHERE md5 = '$md5' AND sha1 = '$sha1'");
        if ($query->num_rows > 0) {
            unlink($fileTmpLoc);
            $row = $query->fetch_assoc();
            $src = $row['src'];
            $type = $row['type'];
            $this->conn->query("UPDATE attachments SET last_updated = NOW() WHERE src = '$src' AND type = '$type'");
            return $row;
        }
        $__fileExt = explode(".", $fileName);
        $_fileExt = end($__fileExt);
        $fileExt = strtolower($_fileExt);
        $newSrc = uniqid('',true);
        if ($w >= $h && $w > 2000) {
            $newW = 2000;
            $newH = (int) 2000 * $h/$w;
        } elseif ($h >= $w && $h > 2000){
            $newH = 2000;
            $newW = (int) 2000 * $w/$h;
        } else {
            $newW = $w;
            $newH = $h;
        }
        $result['src'] = $newSrc;
        if (in_array($fileExt, ['jpg', 'jpeg'])) {
            $img = imagecreatefromjpeg($fileTmpLoc);
            if (!$img) {
                unlink($fileTmpLoc);
                return false;
            }
            $this->conn->query("INSERT INTO attachments (src, type, md5, sha1, last_updated) VALUES ('$newSrc', 'jpg', '$md5', '$sha1', NOW())");
            $newLocation = $_SERVER['DOCUMENT_ROOT'].'/useruploads/'.$newSrc.".jpg";
            $tci = imagecreatetruecolor($newW, $newH);
            imagecopyresampled($tci, $img, 0, 0, 0, 0, $newW, $newH, $w, $h);
            imagejpeg($tci, $newLocation, 84);
            unlink($fileTmpLoc);
            $newMD5 = hash_file('md5', $newLocation);
            $newSHA1 = hash_file('sha1', $newLocation);
            $this->conn->query("INSERT INTO attachments (src, type, md5, sha1, last_updated, additional) VALUES ('$newSrc', 'jpg', '$newMD5', '$newSHA1', NOW(), 1)");
            $result['type'] = 'jpg';
            return $result;
        }
        if ($fileExt === 'png') {
            $img = imagecreatefrompng($fileTmpLoc);
            if (!$img) {
                unlink($fileTmpLoc);
                return false;
            }
            $this->conn->query("INSERT INTO attachments (src, type, md5, sha1, last_updated) VALUES ('$newSrc', 'jpg', '$md5', '$sha1', NOW())");
            $newLocation = $_SERVER['DOCUMENT_ROOT'].'/useruploads/'.$newSrc.".jpg";
            $tci = imagecreatetruecolor($newW, $newH);
            $white = imagecolorallocate ( $tci, 255, 255, 255 );
            imagefill ( $tci, 0, 0, $white );
            imagecopyresampled($tci, $img, 0, 0, 0, 0, $newW, $newH, $w, $h);
            imagejpeg($tci, $newLocation, 84);
            unlink($fileTmpLoc);
            $newMD5 = hash_file('md5', $newLocation);
            $newSHA1 = hash_file('sha1', $newLocation);
            $this->conn->query("INSERT INTO attachments (src, type, md5, sha1, last_updated, additional) VALUES ('$newSrc', 'jpg', '$newMD5', '$newSHA1', NOW(), 1)");
            $result['type'] = 'jpg';
            return $result;
        }
        if ($fileExt === "gif") {
            if ($w > 2000 || $h > 2000) {
                unlink($fileTmpLoc);
                return false;
            }
            $newLocation = $_SERVER['DOCUMENT_ROOT'].'/useruploads/'.$newSrc.".gif";
            $thumbLocation = $_SERVER['DOCUMENT_ROOT'].'/useruploads/'.$newSrc.".jpg";
            move_uploaded_file($fileTmpLoc, $newLocation);
            $img = imagecreatefromgif($newLocation);
            if (!$img) {
                unlink($newLocation);
                return false;
            }
            $this->conn->query("INSERT INTO attachments (src, type, md5, sha1, last_updated) VALUES ('$newSrc', 'gif', '$md5', '$sha1', NOW())");
            $tci = imagecreatetruecolor($newW, $newH);
            $white = imagecolorallocate ( $tci, 255, 255, 255 );
            imagefill ( $tci, 0, 0, $white );
            imagecopyresampled($tci, $img, 0, 0, 0, 0, $newW, $newH, $w, $h);
            imagejpeg($tci, $thumbLocation, 84);
            $result['type'] = "gif";
            return $result;
        }
    }

    private function saveUserpic($file) {
        $fileName = $file["name"];
        $fileTmpLoc = $file["tmp_name"];
        $fileSize = $file["size"];
        $__fileExt = explode(".", $fileName);
        $_fileExt = end($__fileExt);
        $fileExt = strtolower($_fileExt);
        $newSrc = uniqid('',true);
        $newLocation = $_SERVER['DOCUMENT_ROOT'].'/userpics/'.$newSrc.".jpg";
        list($w, $h) = getimagesize($fileTmpLoc);
        if (in_array($fileExt, ['jpg', 'jpeg'])) {
            $img = imagecreatefromjpeg($fileTmpLoc);
        }
        if (in_array($fileExt, ['png', 'gif'])) {
            $imagecreatefrom = "imagecreatefrom".$fileExt;
            $img = $imagecreatefrom($fileTmpLoc);
        }
        $tci = imagecreatetruecolor(220, 220);
        $white = imagecolorallocate ( $tci, 255, 255, 255 );
        imagefill ( $tci, 0, 0, $white );
        imagecopyresampled($tci, $img, 0, 0, 0, 0, 220, 220, $w, $h);
        imagejpeg($tci, $newLocation, 84);
        unlink($fileTmpLoc);
        $result['src'] = $newSrc;
        return $result;
    }

    private function changeUserPic($file) {
        if($this->checkErrors($file)) {
            $this->result = null;
            return;
        }
        $savedFile = $this->saveUserpic($file);
        $userpic = $savedFile['src'];
        $this->deleteUserPic();
        $this->conn->query("UPDATE users SET userpic = '$userpic' WHERE id = $this->user");
        $_SESSION['userpic'] = $userpic;
        $this->result = true;
        $this->data['userpic'] = $userpic;
    }

    private function deleteUserPic() {
        $query = $this->conn->query("SELECT userpic FROM users WHERE id = $this->user");
        if($query->num_rows == 0){
            $this->result = null;
            return;
        }
        $row = $query->fetch_assoc();
        if (!$row['userpic']) {
            return;
        }
        unlink($_SERVER['DOCUMENT_ROOT']."/userpics/".$row['userpic'].".jpg");
        $this->conn->query("UPDATE users SET userpic = NULL WHERE id = $this->user");
        $_SESSION['userpic'] = null;
        $this->result = true;
    }

    public function response() {
        $response['data'] = $this->data;
        $response['result'] = $this->result;
        $response['error'] = $this->error;
        return $response;
    }

    public function __destruct() {
        if ($this->conn) {
            $this->conn->close();
        }
    }
}
