<?php
include_once 'db-conn.php';
include_once 'common-functions.php';
include_once 'config.php';

class SignUpHandler extends DBConn {
    private $result;
    private $error;
    private $conn;
    public function __construct($form) {
        if (!isset($form['query'])) {
            $this->result = null;
            return;
        }
        $this->conn = $this->connect();
        switch($form['query']) {
            case 'check_username':
                if (!isset($form['username'])) {
                    $this->result = null;
                    return;
                }
                $this->checkUsername($form['username']);
                break;
            case 'check_email':
                if (!isset($form['email'])) {
                    $this->result = null;
                    return;
                }
                $this->checkEmail($form['email']);
                break;
            case 'sign_up':
                if (!isset($form['username']) || !isset($form['email']) || !isset($form['password']) || !isset($form['captcha'])) {
                    $this->result = null;
                    return;
                }
                if($this->checkForm($form)) {
                    $this->signUp($form);
                }
                break;
            case 'sign_up_vk':
                if (!isset($form['username']) || !isset($form['expire']) || !isset($form['mid']) || !isset($form['secret']) || !isset($form['sid']) || !isset($form['sig'])) {
                    $this->result = null;
                    return;
                }
                $this->signUpVk($form);
                break;
            case 'sign_up_google':
                if (!isset($form['username']) || !isset($form['id_token'])) {
                    $this->result = null;
                    return;
                }
                $this->signUpGoogle($form);
                break;
            case 'verification':
                if (!isset($form['code'])) {
                    $this->result = null;
                    return;
                }
                $this->verification($form);
                break;
            case 'recreate_password_init':
                if (!isset($form['email']) || !isset($form['captcha'])) {
                    $this->result = null;
                    return;
                }
                $this->recreatePasswordInit($form);
                break;
            case 'recreate_password_end':
                if (!isset($form['user_id']) || !isset($form['recreation_password']) || !isset($form['new_password']) || !isset($form['captcha'])) {
                    $this->result = null;
                    return;
                }
                $this->recreatePasswordEnd($form);
                break;
            default: $this->result = null;
                return;
        }

    }
    private function signUpGoogle($form) {
        if(!$this->checkUsername($form['username'])) {
            return;
        }
        $google_id = CommonFunctions::googleAuth($form['id_token']);
        if (!$google_id) {
            $this->result = null;
            return;
        }
        $tp_id = $this->conn->real_escape_string($google_id);
        $query = $this->conn->query("SELECT id FROM users WHERE tp_id = '$tp_id' AND acc_type = 'google'");
        if ($query->num_rows > 0) {
            $this->error = "already_registered";
            $this->result = null;
            return;
        }
        $stmt = $this->conn->prepare("INSERT INTO users (username, tp_id, status, registration, acc_type) VALUES (?, ?, 'normal', NOW(), 'google')");
        $stmt->bind_param('ss', $form['username'], $tp_id);
        $stmt->execute();
        $stmt->close();
        $this->result = true;
    }
    private function signUpVk($form) {
        if(!$this->checkUsername($form['username'])) {
            return;
        }
        $check = "expire=".$form['expire']."mid=".$form['mid']."secret=".$form['secret']."sid=".$form['sid'].VK_SECRET;
        if ($form['sig'] !== md5($check)) {
            $this->result = null;
            return;
        }
        if (mb_strlen($form['mid']) > 128) {
            $this->result = null;
            return;
        }
        $tp_id = $this->conn->real_escape_string($form['mid']);
        $query = $this->conn->query("SELECT id FROM users WHERE tp_id = '$tp_id' AND acc_type = 'vk'");
        if ($query->num_rows > 0) {
            $this->error = "already_registered";
            $this->result = null;
            return;
        }
        $stmt = $this->conn->prepare("INSERT INTO users (username, tp_id, status, registration, acc_type) VALUES (?, ?, 'normal', NOW(), 'vk')");
        $stmt->bind_param('ss', $form['username'], $tp_id);
        $stmt->execute();
        $stmt->close();
        $this->result = true;
    }
    private function signUp($form) {
        $verification = CommonFunctions::generateRandomString(128);
        $username = $form['username'];
        $stmt = $this->conn->prepare("INSERT INTO users (username, email, password, verification, status, registration, acc_type) VALUES (?,?,?,?,'new',NOW(), 'normal')");
        $stmt->bind_param("ssss", $username, $form['email'], $hashedPwd, $verification);
        $hashedPwd = password_hash($form['password'], PASSWORD_DEFAULT);
        $stmt->execute();
        $stmt->close();
        $this->result = true;
        $message = "Hello $username! To finish registration process click the following link\r\nhttps://".MY_SITE_NAME."/verify/".$verification." \r\nПривет $username! Для завершения регистрации перейдите по ссылке выше" ;
        $to = $form['email'];
        $subject = MY_SITE_NAME." sign up";
        $from = NOTIFICATION_FROM;
        CommonFunctions::sendMail($from, $to, $subject, $message);
    }
    private function checkForm($form) {
        if (!CommonFunctions::checkCaptcha($form['captcha'])) {
            return false;
        }
        if (!$this->checkUsername($form['username'])) {
            return false;
        }
        if (!$this->checkEmail($form['email'])) {
            return false;
        }
        $password = (string)$form['password'];
        if (mb_strlen($password) < 6 || mb_strlen($password) > 32) {
            $this->error = "invalid_password";
            $this->result = null;
            return false;
        }
        $this->result = false; // checkUsername and checkEmail switched result to true, but it's too early in this case
        return true;
    }
    private function checkUsername($username) {
        $username = (string)$username;
        if(mb_strlen($username) < 4 && mb_strlen($username) > 32) {
            $this->error = "invalid_username";
            $this->result = null;
            return false;
        }
        if(!preg_match("/^[a-zA-Z0-9а-яА-ЯёЁ_\-]*$/u", $username)) {
            $this->error = "invalid_username";
            $this->result = null;
            return false;
        }
        $stmt = $this->conn->prepare("SELECT id FROM users WHERE username=?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0) {
            $this->error = "username_exists";
            $this->result = null;
            $stmt->close();
            return false;
        }
        $this->result = true;
        $stmt->close();
        return true;
    }

    private function checkEmail($email) {
        $email = (string)$email;
        if(mb_strlen($email) < 6 && mb_strlen($email) > 64) {
            $this->error = "invalid_email";
            $this->result = null;
            return false;
        }
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error = "invalid_email";
            $this->result = null;
            return false;
        }
        $stmt = $this->conn->prepare("SELECT id FROM users WHERE email=?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0) {
            $this->error = "email_exists";
            $this->result = null;
            $stmt->close();
            return false;
        }
        $this->result = true;
        $stmt->close();
        return true;
    }
    private function verification($form) {
        $verification_code = (string)$form['code'];
        if (!preg_match('/^[a-z0-9A-Z]{128}$/u', $verification_code)) {
            $this->result = null;
            return;
        }
        $query = $this->conn->query("SELECT id FROM users WHERE verification = '$verification_code'");
        if ($query->num_rows == 0) {
            $this->result = null;
            return;
        }
        $this->conn->query("UPDATE users SET status = 'normal', verification = NULL WHERE verification = '$verification_code'");
        $this->result = true;
    }
    private function recreatePasswordInit($form) {
        if (!CommonFunctions::checkCaptcha($form['captcha'])) {
            $this->result = null;
            return;
        }
        if (mb_strlen($form['email']) < 6 && mb_strlen($form['email']) > 64) {
            $this->result = null;
            return;
        }
        if(!filter_var($form['email'], FILTER_VALIDATE_EMAIL)) {
            $this->result = null;
            return;
        }
        $email = $this->conn->real_escape_string($form['email']);
        $recreation_password = CommonFunctions::generateRandomString(128);
        $query = $this->conn->query("SELECT id, username FROM users WHERE email = '$email' AND acc_type = 'normal'");
        if ($query->num_rows == 0) {
            $this->result = null;
            return;
        }
        $row = $query->fetch_assoc();
        $user_id = $row['id'];
        $hashedRecreationPwd = password_hash($recreation_password, PASSWORD_DEFAULT);
        $this->conn->query("UPDATE users SET recreation_password = '$hashedRecreationPwd', recreation_date = NOW() WHERE id = $user_id");
        $message = "Hello ".$row['username']."! To recreate your password click the following link\r\nhttps://".MY_SITE_NAME."/recreate_password/".$recreation_password."/".$user_id." \r\nПривет ".$row['username']."! Для восстановления пароля перейдите по ссылке выше";
        $to = $email;
        $subject = MY_SITE_NAME." password recreation";
        $from = NOTIFICATION_FROM;
        CommonFunctions::sendMail($from, $to, $subject, $message);
        $this->result = true;
    }
    private function recreatePasswordEnd($form) {
        if (!CommonFunctions::checkCaptcha($form['captcha'])) {
            $this->result = null;
            return;
        }
        $user_id = (int)$form['user_id'];
        $recreation_password = $form['recreation_password'];
        $new_password = (string)$form['new_password'];
        if(mb_strlen($new_password) < 6 && mb_strlen($new_password) > 32) {
            $this->result = null;
            return;
        }
        $query = $this->conn->query("SELECT recreation_password FROM users WHERE id = $user_id AND recreation_password IS NOT NULL");
        if ($query->num_rows == 0) {
            $this->result = null;
            return;
        }
        $row = $query->fetch_assoc();
        if(!password_verify($recreation_password, $row['recreation_password'])) {
            $this->result = null;
            return;
        }
        $hashedPwd = password_hash($new_password, PASSWORD_DEFAULT);
        $this->conn->query("UPDATE users SET password = '$hashedPwd', recreation_password = NULL, recreation_date = NULL WHERE id = $user_id");
        $this->result = true;
    }
    public function response() {
        $result['result'] = $this->result;
        $result['error'] = $this->error;
        return $result;
    }
    public function __destruct() {
        if ($this->conn) {
            $this->conn->close();
        }
    }
}
