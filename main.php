<?php
mb_internal_encoding("UTF-8");
include_once 'json-handler.php';
include_once 'admin.php';
include_once 'collection-manager.php';
include_once 'content-creator.php';
include_once 'content-loader.php';
include_once 'image-manager.php';
include_once 'profile-manager.php';
include_once 'session.php';
include_once 'sign-up-handler.php';

if (!isset($_POST['json'])) {
    $response['result'] = null;
    echo json_encode($response);
    exit;
}
$json = new JSONHandler($_POST['json']);
if(!$json->result) {
    $response['result'] = null;
    echo json_encode($response);
    exit;
}
$form = $json->result;
if(!isset($form['query'])) {
    $response['result'] = null;
    echo json_encode($response);
    exit;
}

$session = new Session();

switch($form['query']) {
    case 'ban':
    case 'unban':
    case 'warn_user':
    case 'warn_comment':
    case 'edit':
    case 'delete_attachment':
    case 'delete_post':
    case 'delete_comment':
    case 'delete_all':
    case 'change_post_attributes':
    case 'fetch_complaints':
    case 'delete_complaint':
    case 'delete_user_complaints':
        $session->checkSession();
        $admin = new Admin($form);
        $response = $admin->response();
        break;
    case 'add_tag':
    case 'delete_tag':
    case 'vote':
    case 'favorite':
    case 'ignore':
    case 'subscribe':
    case 'block_tag':
    case 'unlock_tag':
        $session->checkSession();
        $collectionManager = new CollectionManager($form);
        $response['result'] = $collectionManager->result;
        break;
    case 'create_post':
    case 'create_comment':
        $session->checkSession();
        $contentCreator = new ContentCreator($form);
        $response['result'] = $contentCreator->result;
        break;
    case 'feed':
    case 'favposts':
    case 'subscriptions':
    case 'search':
    case 'post':
    case 'postcomments':
    case 'messages':
    case 'favcomments':
    case 'usercomments':
    case 'blocked_tags':
    case 'ignored_users':
    case 'subscribed_users':
    case 'users':
    case 'user':
    case 'check_new_messages':
        $session->easySessionCheck();
        $contentLoader = new ContentLoader($form);
        $response = $contentLoader->response();
        break;
    case 'upload_images':
    case 'change_userpic':
    case 'delete_userpic':
        $session->checkSession();
        $imageManager = new ImageManager($form);
        $response = $imageManager->response();
        break;
    case 'apply_settings':
    case 'change_description':
    case 'complain':
    case 'delete_message':
    case 'reset_messages':
    case 'change_password':
        $session->checkSession();
        $profileManager = new ProfileManager($form);
        $response = $profileManager->response();
        break;
    case 'check_username':
    case 'check_email':
    case 'sign_up':
    case 'sign_up_vk':
    case 'sign_up_google':
    case 'verification':
    case 'recreate_password_init':
    case 'recreate_password_end':
        $signUpHandler = new SignUpHandler($form);
        $response = $signUpHandler->response();
        break;
    case 'log_in':
        $session->checkSession();
        $response = $session->logIn($form);
        break;
    case 'log_in_vk':
        $session->checkSession();
        $response = $session->logInVk($form);
        break;
    case 'log_in_google':
        $session->checkSession();
        $response = $session->logInGoogle($form);
        break;
    case 'log_out':
        $session->checkSession();
        $response = $session->logOut();
        break;
    case 'active_sessions':
        $session->checkSession();
        $response = $session->fetchActiveSessions();
        break;
    case 'log_out_everywhere':
        $session->checkSession();
        $response = $session->logOutEverywhere();
        break;
    case 'check_session':
        $response = $session->checkSession();
        break;
    default:
        $response['result'] = null;
}
$response['authorized'] = (bool)$session->user;
echo json_encode($response);
exit;
