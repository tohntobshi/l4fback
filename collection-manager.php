<?php
include_once 'db-conn.php';

class CollectionManager extends DBConn {
    private $user;
    private $conn;
    public $result;
//------------------------------------------------------------------------------
    public function __construct($form) {
        $this->user = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
        if ($this->user == 0) {
            $this->result = null;
            return;
        }
        if (!isset($form['query'])) {
            $this->result = null;
            return;
        }
        $this->conn = $this->connect();
        switch ($form['query']) {
            //----------------------------------------------------------------------
            case 'vote':
                if (!isset($form['id']) || !isset($form['type']) || !isset($form['vote'])) {
                    $this->result = null;
                    return;
                }
                $this->vote($form);
                break;
            //----------------------------------------------------------------------
            case 'add_tag':
                if (!isset($form['id']) || !isset($form['tag'])) {
                    $this->result = null;
                    return;
                }
                $this->addTag($form);
                break;
            //----------------------------------------------------------------------
            case 'delete_tag':
                if (!isset($form['id']) || !isset($form['tag'])) {
                    $this->result = null;
                    return;
                }
                $this->deleteTag($form);
                break;
            //----------------------------------------------------------------------
            case 'favorite':
                if (!isset($form['id']) || !isset($form['type'])) {
                    $this->result = null;
                    return;
                }
                $this->addToFav($form);
                break;
            //----------------------------------------------------------------------
            case 'ignore':
            case 'subscribe':
                if (!isset($form['username'])) {
                    $this->result = null;
                    return;
                }
                $this->ignoreOrSubscribe($form);
                break;
            //----------------------------------------------------------------------
            case 'block_tag':
                if (!isset($form['tag'])) {
                    $this->result = null;
                    return;
                }
                $this->blockTag($form);
                break;
            //----------------------------------------------------------------------
            case 'unlock_tag':
                if (!isset($form['tag'])) {
                    $this->result = null;
                    return;
                }
                $this->unlockTag($form);
                break;
            //----------------------------------------------------------------------
            default:
                $this->result = null;
                return;
      }
    }
//------------------------------------------------------------------------------
    private function vote($form) {
        $id = (int)$form['id'];
        $type = $form['type'];
        $vote = $form['vote'] ? 1 : 0;
        if ($type != 'post' && $type != 'comment') {
            $this->result = null;
            return;
        }
        $result = $this->conn->query("SELECT * FROM $type"."rates WHERE $type = $id AND user = $this->user");
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            if ($row['sign'] == $vote) {
                $this->conn->query("DELETE FROM $type"."rates WHERE $type = $id AND user = $this->user");
                $this->result = null;
            } else {
                $this->conn->query("UPDATE $type"."rates SET sign = $vote WHERE $type = $id AND user = $this->user");
                $this->result = (bool)$vote;
            }
        } else {
            $this->conn->query("INSERT INTO $type"."rates ($type, user, sign) VALUES ($id, $this->user, $vote)");
            $this->result = (bool)$vote;
        }
    }
//------------------------------------------------------------------------------
    private function addToFav($form) {
        $id = (int)$form['id'];
        $type = $form['type'];
        if ($type != 'post' && $type != 'comment') {
            $this->result = null;
            return;
        }
        $result = $this->conn->query("SELECT user, $type FROM favorite_".$type."s WHERE $type = $id AND user = $this->user");
        if ($result->num_rows > 0) {
            $this->conn->query("DELETE FROM favorite_".$type."s WHERE $type = $id AND user = $this->user");
            $this->result = false;
        } else {
            $this->conn->query("INSERT INTO favorite_".$type."s ($type, user) VALUES ($id, $this->user)");
            $this->result = true;
        }
    }
//------------------------------------------------------------------------------
    private function blockTag($form) {
        $stmt = $this->conn->prepare(
            "INSERT INTO banned_tags (tag, user) SELECT (SELECT id FROM tags WHERE tag = ?) AS tag, ? AS user FROM dual WHERE NOT EXISTS
            (SELECT * FROM banned_tags WHERE tag = (SELECT id FROM tags WHERE tag = ?) AND user = ?)");
            $stmt->bind_param('sisi', $form['tag'], $this->user, $form['tag'], $this->user);
            $stmt->execute();
            $stmt->close();
            $this->result = true;
        }
//------------------------------------------------------------------------------
    private function unlockTag($form) {
        $stmt = $this->conn->prepare("DELETE FROM banned_tags WHERE tag = (SELECT id FROM tags WHERE tag = ?) AND user = ?");
        $stmt->bind_param('si', $form['tag'], $this->user);
        $stmt->execute();
        $stmt->close();
        $this->result = true;
    }
//------------------------------------------------------------------------------
    private function ignoreOrSubscribe($form) {
        $type = ($form['query'] == 'ignore') ? 'ignored_users' : 'subscriptions';
        $stmt = $this->conn->prepare("SELECT id FROM ".$type." WHERE user = ? AND target_user = (SELECT id FROM users WHERE username = ?)");
        $stmt->bind_param('is', $this->user, $form['username']);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $stmt2 = $this->conn->prepare("DELETE FROM ".$type." WHERE user = ? AND target_user = (SELECT id FROM users WHERE username = ?)");
            $stmt2->bind_param('is', $this->user, $form['username']);
            $stmt2->execute();
            $this->result = false;
        } else {
            $stmt2 = $this->conn->prepare("INSERT INTO ".$type." (user, target_user) VALUES (?, (SELECT id FROM users WHERE username = ?))");
            $stmt2->bind_param('is', $this->user, $form['username']);
            $stmt2->execute();
            $this->result = true;
        }
        $stmt->close();
        $stmt2->close();
    }
//------------------------------------------------------------------------------
    private function addTag($form) {
        $id = (int)$form['id'];
        $tag = $form['tag'];
        if (mb_strlen($tag) > 32 || mb_strlen($tag) < 2 || !preg_match("/^[a-z0-9а-я\- ]*$/u", $tag)) {
            $this->result = null;
            return;
        }
        $query = $this->conn->query("SELECT user FROM posts WHERE id = $id");
        if ($query->num_rows === 0) {
            $this->result = null;
            return;
        }
        $row = $query->fetch_assoc();
        $post_author_id = (int)$row['user'];
        if ($_SESSION['status'] != 'admin' && $_SESSION['user_id'] != $post_author_id) {
            $this->result = null;
            return;
        }
        $this->conn->query("INSERT INTO tags (tag) SELECT '$tag' AS tag FROM dual WHERE NOT EXISTS(SELECT tag FROM tags WHERE tag = '$tag')");
        $this->conn->query("INSERT INTO tagmap (tag, post) SELECT (SELECT id FROM tags WHERE tag = '$tag') AS tag, $id AS post FROM dual WHERE (SELECT COUNT(tag) FROM tagmap WHERE post = $id) < 10");
        $this->result = true;
    }
//------------------------------------------------------------------------------
    private function deleteTag($form) {
        $id = (int)$form['id'];
        $tag = $this->conn->real_escape_string($form['tag']);
        $query = $this->conn->query("SELECT user FROM posts WHERE id = $id");
        if ($query->num_rows === 0) {
            $this->result = null;
            return;
        }
        $row = $query->fetch_assoc();
        $post_author_id = (int)$row['user'];
        if ($_SESSION['status'] != 'admin' && $_SESSION['user_id'] != $post_author_id) {
            $this->result = null;
            return;
        }
        $this->conn->query("DELETE FROM tagmap WHERE post = $id AND tag = (SELECT id FROM tags WHERE tag = '$tag')");
        if ($this->conn->affected_rows < 1) {
            $this->result = null;
            return;
        }
        $this->result = true;
    }
//------------------------------------------------------------------------------
    public function __destruct() {
        if ($this->conn) {
            $this->conn->close();
        }
    }
}
