<?php

class JSONHandler {
  public $result = false;
  public function __construct($json = NULL) {
    $this->result = $this->jsonHandler($json);
  }

  private function jsonHandler($json = NULL) {
    if ($json) {
      $form = json_decode($json, true);
      if (json_last_error_msg()) {
        return $form;
      } else { return false; }
    } else { return false; }
  }

}
