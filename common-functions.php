<?php
include_once 'json-handler.php';
include_once 'config.php';
require_once "Mail.php";

class CommonFunctions {
    static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function checkCaptcha($captcha) {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $params = array(
            'secret' => RECAPTCHA_SECRET,
            'response' => $captcha,
            'remoteip' => $_SERVER['REMOTE_ADDR']
        );
        $response = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
        $result = new JSONHandler($response);
        if (!$result->result) {
            return false;
        }
        if (!isset($result->result['success'])) {
            return false;
        }
        return (bool)$result->result['success'];
    }
    static function googleAuth($token) {
        if (!preg_match("/^[a-zA-Z\-\._0-9]{1,2000}$/u", $token)) {
            return false;
        }
        $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$token;
        $response = file_get_contents($url);
        $json = new JSONHandler($response);
        $result = $json->result;
        if (!$result) {
            return false;
        }
        if (!isset($result['aud'])) {
            return false;
        }
        if ($result['aud'] !== GOOGLE_CLIENT_ID) {
            return false;
        }
        if (!isset($result['sub'])) {
            return false;
        }
        return $result['sub'];
    }
    static function sendMail($from, $to, $subject, $body) {
        $headers = array (
            'From' => $from,
            'To' => $to,
            'Subject' => $subject
        );
        $smtp = Mail::factory('smtp', array (
            'host' => MY_MAIL_HOST,
            'port' => MY_MAIL_PORT,
            'auth' => true,
            'username' => MY_MAIL_USERNAME,
            'password' => MY_MAIL_PASSWORD
        ));
        $mail = $smtp->send($to, $headers, $body);
        return PEAR::isError($mail);
    }
}
