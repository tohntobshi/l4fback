<?php
include_once 'config.php';

class DBConn {
  protected function connect() {
    $servername = 'localhost';
    $username = DB_USERNAME;
    $password = DB_PASSWORD;
    $dbname = DB_NAME;
    $conn = new mysqli($servername, $username, $password, $dbname);
    return $conn;
  }
}
