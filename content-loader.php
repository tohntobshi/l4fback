<?php
include_once 'db-conn.php';

class ContentLoader extends DBConn {
    private $user;
    private $conn;
    private $data;
    private $result;
    private $numposts;
//--------------------------------------------------------------------------------------------------------------------------------
    public function __construct($form) {
        if (!isset($form['query'])) {
            $this->result = null;
            return;
        }
        $this->user = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
        $this->numposts = isset($_SESSION['numposts']) ? ($_SESSION['scrolltype'] == 'pagination' ? $_SESSION['numposts'] : 10) : 10;
        $this->show_terror = isset($_SESSION['show_terror']) ? $_SESSION['show_terror'] : false;
        $this->conn = $this->connect();
        switch ($form['query']) {
            //------------------------------------------------------------------------
            case 'feed':
                if (!isset($form['last_id'])) {
                    $this->result = null;
                    return;
                }
                $this->fetchPosts($form);
                break;
            //------------------------------------------------------------------------
            case 'favposts':
            case 'subscriptions':
                if (!isset($form['last_id']) || $this->user == 0) {
                    $this->result = null;
                    return;
                }
                $this->fetchPosts($form);
                break;
            //------------------------------------------------------------------------
            case 'search':
                if (!isset($form['last_id']) || !isset($form['searchwords']) || !isset($form['searchtype'])) {
                    $this->result = null;
                    return;
                }
                $this->fetchPosts($form);
                break;
            //------------------------------------------------------------------------
            case 'post':
                if (!isset($form['post_id'])) {
                    $this->result = null;
                    return;
                }
                if ($this->fetchPost($form)) {
                    $this->fetchComments($form);
                }
                break;
            //------------------------------------------------------------------------
            case 'postcomments':
                if (!isset($form['post_id']) || !isset($form['last_id'])) {
                    $this->result = null;
                    return;
                }
                $this->fetchComments($form);
                break;
            //------------------------------------------------------------------------
            case 'messages':
            case 'favcomments':
                if (!isset($form['last_id']) || $this->user == 0) {
                    $this->result = null;
                    return;
                }
                $this->fetchComments($form);
                break;
            //------------------------------------------------------------------------
            case 'usercomments':
                if (!isset($form['last_id']) || !isset($form['username']) || $this->user === 0 || $_SESSION['status'] != 'admin') {
                    $this->result = null;
                    return;
                }
                $this->fetchComments($form);
                break;
            //------------------------------------------------------------------------
            case 'blocked_tags':
                if($this->user == 0) {
                    $this->result = null;
                    return;
                }
                $this->fetchBlockedTags();
                break;
            //------------------------------------------------------------------------
            case 'ignored_users':
                if($this->user == 0) {
                    $this->result = null;
                    return;
                }
                $this->fetchIgnoredUsers();
                break;
            //------------------------------------------------------------------------
            case 'subscribed_users':
                if($this->user == 0) {
                    $this->result = null;
                    return;
                }
                $this->fetchSubscribedUsers();
                break;
            //------------------------------------------------------------------------
            case 'users':
                $this->fetchUsers();
                break;
            //------------------------------------------------------------------------
            case 'user':
                if (!isset($form['username'])) {
                    $this->result = null;
                    return;
                }
                $this->fetchUser($form);
                break;
            //------------------------------------------------------------------------
            case 'check_new_messages':
                if($this->user == 0) {
                    $this->result = null;
                    return;
                }
                $this->checkNewMessages();
                break;
            //------------------------------------------------------------------------
            default:
                $this->result = null;
                return;
                break;
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchPosts($form) {
        $last_id = (int)$form['last_id'];
        $type = $form['query'];
        if($type == "search") {
            $type_of_search = $form['searchtype'];
            $words_to_search = mb_substr($form['searchwords'], 0, 64);
            $words_to_search = $this->conn->real_escape_string($words_to_search);
        }
        if (!$this->user) {
            $restriction = "AND posts.terror = 0 AND posts.auth_only = 0";
        } elseif ($this->user && !$this->show_terror) {
            $restriction = "AND posts.terror = 0";
        } else {
            $restriction = "";
        }
        switch (true) {
            case ($type == 'feed' && $last_id):
                $coresql = "SELECT posts.*, posts.id AS sep_id FROM posts WHERE id NOT IN (SELECT tagmap.post FROM tagmap, banned_tags WHERE banned_tags.tag = tagmap.tag AND banned_tags.user = $this->user) AND id < $last_id ".$restriction." ORDER BY id DESC LIMIT $this->numposts";
                $order = "posts.id DESC;";
                break;
            case ($type == 'feed' && !$last_id ):
                $coresql = "SELECT posts.*, posts.id AS sep_id FROM posts WHERE id NOT IN (SELECT tagmap.post FROM tagmap, banned_tags WHERE banned_tags.tag = tagmap.tag AND banned_tags.user = $this->user) ".$restriction." ORDER BY id DESC LIMIT $this->numposts";
                $order = "posts.id DESC;";
                break;
            case ($type == 'favposts' && $last_id):
                $coresql = "SELECT posts.*, favorite_posts.id AS sep_id FROM posts INNER JOIN favorite_posts ON posts.id = favorite_posts.post WHERE favorite_posts.user = $this->user AND favorite_posts.id < $last_id ORDER BY sep_id DESC LIMIT $this->numposts";
                $order = "posts.sep_id DESC;";
                break;
            case ($type == 'favposts' && !$last_id):
                $coresql = "SELECT posts.*, favorite_posts.id AS sep_id FROM posts INNER JOIN favorite_posts ON posts.id = favorite_posts.post WHERE favorite_posts.user = $this->user ORDER BY sep_id DESC LIMIT $this->numposts";
                $order = "posts.sep_id DESC;";
                break;
            case ($type == 'search' && $type_of_search == 'title'):
                $this->conn->query("SET @var = 0");
                $coresql = "SELECT * FROM (SELECT (@var:=@var+1) AS sep_id, posts.*, MATCH(posts.title) AGAINST ('$words_to_search') AS rel FROM posts WHERE MATCH(posts.title) AGAINST ('$words_to_search')) AS posts WHERE sep_id > $last_id ".$restriction." LIMIT $this->numposts";
                $order = "posts.sep_id;";
                break;
            case ($type == 'search' && $type_of_search == 'tags'):
                if (!$this->user) {
                    $restriction = "WHERE posts.terror = 0 AND posts.auth_only = 0";
                } elseif ($this->user && !$this->show_terror) {
                    $restriction = "WHERE posts.terror = 0";
                } else {
                    $restriction = "";
                }
                $this->conn->query("SET @var = 0");
                $coresql =
                    "SELECT posts.*, tagmatches.sep_id FROM posts INNER JOIN
                        (SELECT * FROM
                            (SELECT (@var := @var + 1) AS sep_id, tagmatches.post FROM
                                (SELECT tagmap.post, COUNT(tagmap.post) as matches FROM tagmap WHERE tagmap.tag IN (SELECT id FROM tags WHERE MATCH(tag) AGAINST('$words_to_search')) GROUP BY tagmap.post ORDER BY matches DESC, tagmap.post DESC)
                            AS tagmatches)
                        AS tagmatches WHERE sep_id > $last_id)
                    AS tagmatches ON posts.id = tagmatches.post ".$restriction." ORDER BY tagmatches.sep_id LIMIT $this->numposts";
                $order = "posts.sep_id;";
                break;
            case ($type == 'search' && $type_of_search == 'author' && $last_id):
                $coresql = "SELECT posts.id AS sep_id, posts.* FROM posts WHERE posts.user = (SELECT id FROM users WHERE username = '$words_to_search') AND posts.id < $last_id ".$restriction." ORDER BY posts.id DESC LIMIT $this->numposts";
                $order = "posts.id DESC;";
                break;
            case ($type == 'search' && $type_of_search == 'author' && !$last_id):
                $coresql = "SELECT posts.id AS sep_id, posts.* FROM posts WHERE posts.user = (SELECT id FROM users WHERE username = '$words_to_search') ".$restriction." ORDER BY posts.id DESC LIMIT $this->numposts";
                $order = "posts.id DESC;";
                break;
            case ($type == 'subscriptions' && $last_id):
                $coresql = "SELECT posts.*, posts.id AS sep_id FROM posts WHERE posts.user IN (SELECT target_user FROM subscriptions WHERE user = $this->user) AND posts.id < $last_id ".$restriction." ORDER BY posts.id DESC LIMIT $this->numposts";
                $order = "posts.id DESC;";
                break;
            case ($type == 'subscriptions' && !$last_id):
                $coresql = "SELECT posts.*, posts.id AS sep_id FROM posts WHERE posts.user IN (SELECT target_user FROM subscriptions WHERE user = $this->user) ".$restriction." ORDER BY posts.id DESC LIMIT $this->numposts";
                $order = "posts.id DESC;";
                break;
            default:
                $this->result = null;
                return;
        }
        $sql =
            "SELECT posts.*, COUNT(comments.id) AS comments FROM
                (SELECT posts.*, IF(currentusersaves.post, 1, null) AS currentusersaved FROM
                    (SELECT posts.*, COUNT(favorite_posts.id) AS saved FROM
                        (SELECT posts.*, currentuserrates.sign AS currentuserrate FROM
                            (SELECT posts.id, posts.sep_id, posts.author, posts.title, posts.description, posts.date, posts.attachments, posts.main_att, posts.ratingup, posts.ratingdown, GROUP_CONCAT(tags.tag SEPARATOR ',') AS tags FROM
                                (SELECT posts.*, tagmap.tag FROM
                                    (SELECT posts.id, posts.sep_id, posts.author, posts.title, posts.description, posts.date, posts.main_att, posts.ratingup, posts.ratingdown, GROUP_CONCAT( CONCAT(attachments.src,':',attachments.type) ORDER BY posts.att_order ASC) AS attachments FROM
                                        (SELECT posts.*, attachment_map.attachment, attachment_map.id AS att_order FROM
                                            (SELECT posts.*, COUNT(case postrates.sign when 1 then 1 else null end) AS ratingup, COUNT(case postrates.sign when 0 then 1 else null end) AS ratingdown FROM
                                                (SELECT posts.id, posts.title, posts.description, posts.date, posts.main_att, posts.sep_id, users.username AS author FROM
                                                    (".$coresql.") AS posts
                                                LEFT JOIN users ON users.id = posts.user) AS posts
                                            LEFT JOIN postrates ON posts.id = postrates.post GROUP BY posts.id) AS posts
                                        LEFT JOIN attachment_map ON posts.id = attachment_map.post) AS posts
                                    LEFT JOIN attachments ON posts.attachment = attachments.id GROUP BY posts.id) AS posts
                                LEFT JOIN tagmap ON posts.id = tagmap.post) AS posts
                            LEFT JOIN tags ON posts.tag = tags.id GROUP BY posts.id) AS posts
                        LEFT JOIN (SELECT sign, post FROM postrates WHERE user = $this->user) AS currentuserrates ON posts.id = currentuserrates.post) AS posts
                    LEFT JOIN favorite_posts ON posts.id = favorite_posts.post GROUP BY posts.id) AS posts
                LEFT JOIN (SELECT post FROM favorite_posts WHERE user = $this->user) AS currentusersaves ON posts.id = currentusersaves.post GROUP BY posts.id) AS posts
            LEFT JOIN comments ON comments.post = posts.id GROUP BY posts.id ORDER BY ".$order;
            // order by attachments may be necessery
        $query = $this->conn->query($sql);
        if ($query->num_rows === 0) {
            $this->result = null;
            return;
        }
        while ($row = $query->fetch_assoc()) {
            $this->addPost($row);
        }
        $this->result = true;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function addPost($row, $multiple = true) {
        $post['id'] = (int)$row['id'];
        $post['author'] = $row['author'];
        $post['title'] = $row['title'];
        $post['description'] = $row['description'];
        $post['attachments'] = $row['attachments'];
        $post['ratingup'] = (int)$row['ratingup'];
        $post['ratingdown'] = (int)$row['ratingdown'];
        $post['saved'] = (int)$row['saved'];
        $post['tags'] = $row['tags'];
        $post['currentuserrate'] = $row['currentuserrate'] === null ? null : (bool)$row['currentuserrate'];
        $post['currentusersaved'] = (bool)$row['currentusersaved'];
        $post['date'] = $row['date'];
        if ($multiple) {
            $post['sep_id'] = $row['sep_id'] === null ? null : (int)$row['sep_id'];
            $post['main_att'] = (int)$row['main_att'];
            $post['comments'] = (int)$row['comments'];
            $this->data['posts'][] = $post;
        } else {
            $post['hr'] = (bool)$row['hr'];
            $post['auth_only'] = (bool)$row['auth_only'];
            $post['terror'] = (bool)$row['terror'];
            $this->data['post'] = $post;
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchPost($form) {
        $id = (int)$form['post_id'];
        if (!$this->user) {
            $restriction = "AND posts.terror = 0 AND posts.auth_only = 0";
        } elseif ($this->user && !$this->show_terror) {
            $restriction = "AND posts.terror = 0";
        } else {
            $restriction = "";
        }
        $sql =
            "SELECT posts.*, IF(currentusersaves.post, 1, null) AS currentusersaved FROM
                (SELECT posts.*, COUNT(favorite_posts.id) AS saved FROM
                    (SELECT posts.*, currentuserrates.sign AS currentuserrate FROM
                        (SELECT posts.id, posts.author, posts.title, posts.description, posts.date, posts.hr, posts.terror, posts.auth_only, posts.attachments, posts.ratingup, posts.ratingdown, GROUP_CONCAT(tags.tag SEPARATOR ',') AS tags FROM
                            (SELECT posts.*, tagmap.tag FROM
                                (SELECT posts.id, posts.author, posts.title, posts.description, posts.date, posts.hr, posts.terror, posts.auth_only, posts.ratingup, posts.ratingdown, GROUP_CONCAT( CONCAT(attachments.src,':',attachments.type) ORDER BY posts.att_order ASC) AS attachments FROM
                                    (SELECT posts.*, attachment_map.attachment, attachment_map.id AS att_order FROM
                                        (SELECT posts.*, COUNT(case postrates.sign when 1 then 1 else null end) AS ratingup, COUNT(case postrates.sign when 0 then 1 else null end) AS ratingdown FROM
                                            (SELECT posts.id, posts.title, posts.description, posts.date, posts.hr, posts.terror, posts.auth_only, users.username AS author FROM
                                                (SELECT * FROM posts WHERE id = $id ".$restriction.") AS posts
                                            LEFT JOIN users ON users.id = posts.user) AS posts
                                        LEFT JOIN postrates ON posts.id = postrates.post GROUP BY posts.id) AS posts
                                    LEFT JOIN attachment_map ON posts.id = attachment_map.post) AS posts
                                LEFT JOIN attachments ON posts.attachment = attachments.id GROUP BY posts.id) AS posts
                            LEFT JOIN tagmap ON posts.id = tagmap.post) AS posts
                        LEFT JOIN tags ON posts.tag = tags.id GROUP BY posts.id) AS posts
                    LEFT JOIN (SELECT sign, post FROM postrates WHERE user = $this->user AND post = $id) AS currentuserrates ON posts.id = currentuserrates.post) AS posts
                LEFT JOIN favorite_posts ON posts.id = favorite_posts.post GROUP BY posts.id) AS posts
            LEFT JOIN (SELECT post FROM favorite_posts WHERE user = $this->user AND post = $id) AS currentusersaves ON posts.id = currentusersaves.post GROUP BY posts.id";
        $query = $this->conn->query($sql);
        if ($query->num_rows == 0) {
            $this->result = null;
            return false;
        }
        $row = $query->fetch_assoc();
        $this->addPost($row, false);
        $this->result = true;
        return true;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchComments($form) {
        $post_id = (int)$form['post_id'];
        $last_comment_id = isset($form['last_id']) ? (int)$form['last_id'] : 0;

        switch ($form['query']) {
            case 'messages':
                if ($last_comment_id > 0) {
                    $coresql =
                        "SELECT comments.* FROM (SELECT comments.*, messages.id AS sep_id, messages.reference AS mes_ref FROM comments INNER JOIN messages ON comments.id = messages.comment WHERE messages.recipient = $this->user AND messages.id < $last_comment_id ORDER BY sep_id DESC LIMIT $this->numposts) AS comments
                            UNION
                        SELECT comments.* FROM (SELECT comments.*, NULL as sep_id, NULL as mes_ref FROM comments INNER JOIN messages ON messages.reference = comments.id WHERE messages.recipient = $this->user AND messages.id < $last_comment_id ORDER BY messages.id DESC LIMIT $this->numposts) AS comments WHERE comments.id IS NOT NULL";
                } else {
                    $coresql =
                        "SELECT comments.* FROM (SELECT comments.*, messages.id AS sep_id, messages.reference AS mes_ref FROM comments INNER JOIN messages ON comments.id = messages.comment WHERE messages.recipient = $this->user ORDER BY sep_id DESC LIMIT $this->numposts) AS comments
                            UNION
                        SELECT comments.* FROM (SELECT comments.*, NULL as sep_id, NULL as mes_ref FROM comments INNER JOIN messages ON messages.reference = comments.id WHERE messages.recipient = $this->user ORDER BY messages.id DESC LIMIT $this->numposts) AS comments WHERE comments.id IS NOT NULL";
                }
                $order = "comments.sep_id DESC";
                break;
            case 'favcomments':
                if ($last_comment_id > 0) {
                    $coresql = "SELECT comments.*, favorite_comments.id AS sep_id, NULL AS mes_ref FROM comments INNER JOIN favorite_comments ON comments.id = favorite_comments.comment WHERE favorite_comments.user = $this->user AND favorite_comments.id < $last_comment_id ORDER BY sep_id DESC LIMIT $this->numposts";
                } else {
                    $coresql = "SELECT comments.*, favorite_comments.id AS sep_id, NULL AS mes_ref FROM comments INNER JOIN favorite_comments ON comments.id = favorite_comments.comment WHERE favorite_comments.user = $this->user ORDER BY sep_id DESC LIMIT $this->numposts";
                }
                $order = "comments.sep_id DESC";
                break;
            case 'usercomments':
                $username = $this->conn->real_escape_string($form['username']);
                if ($last_comment_id > 0) {
                    $coresql = "SELECT comments.*, comments.id as sep_id, NULL as mes_ref FROM comments WHERE comments.user = (SELECT id FROM users WHERE username = '$username') AND comments.id < $last_comment_id ORDER BY comments.id DESC LIMIT $this->numposts";
                } else {
                    $coresql = "SELECT comments.*, comments.id as sep_id, NULL as mes_ref FROM comments WHERE comments.user = (SELECT id FROM users WHERE username = '$username') ORDER BY comments.id DESC LIMIT $this->numposts";
                }
                $order = "comments.id DESC";
                break;
            default:
                $coresql = "SELECT *, NULL AS sep_id, NULL AS mes_ref FROM comments WHERE post = $post_id AND id > $last_comment_id";
                $order = "comments.id ASC";
                break;
        }
        $sql =
            "SELECT comments.*, IF(currentusersaves.comment, 1, null) AS currentusersaved FROM
                (SELECT comments.*, COUNT(favorite_comments.id) AS saved FROM
                    (SELECT comments.*, currentuserrates.sign AS currentuserrate FROM
                        (SELECT comments.*, GROUP_CONCAT( CONCAT(attachments.src,':',attachments.type) ORDER BY comments.att_order ASC) AS attachments FROM
                            (SELECT comments.*, attachment_map.attachment, attachment_map.id AS att_order FROM
                                (SELECT comments.*, COUNT(case commentrates.sign when 1 then 1 else null end) AS ratingup, COUNT(case commentrates.sign when 0 then 1 else null end) AS ratingdown FROM
                                    (SELECT comments.id, comments.sep_id, comments.mes_ref, comments.comment, comments.date, comments.post, comments.reference, comments.warning, users.username AS author, users.userpic FROM
                                        (".$coresql.") AS comments
                                    LEFT JOIN users ON users.id = comments.user) AS comments
                                LEFT JOIN commentrates ON comments.id = commentrates.comment GROUP BY comments.id) AS comments
                            LEFT JOIN attachment_map ON attachment_map.comment = comments.id) AS comments
                        LEFT JOIN attachments ON comments.attachment = attachments.id GROUP BY comments.id) as comments
                    LEFT JOIN (SELECT sign, comment FROM commentrates WHERE user = $this->user) AS currentuserrates ON comments.id = currentuserrates.comment) AS comments
                LEFT JOIN favorite_comments ON comments.id = favorite_comments.comment GROUP BY comments.id) AS comments
            LEFT JOIN (SELECT comment FROM favorite_comments WHERE user = $this->user) AS currentusersaves ON comments.id = currentusersaves.comment GROUP BY comments.id ORDER BY ".$order;
        $query = $this->conn->query($sql);
        if ($query->num_rows == 0) {
            if ($form['query'] != 'post') {
                $this->result = null;
            }
            return;
        }
        while ($row = $query->fetch_assoc()) {
            $comment['id'] = (int)$row['id'];
            $comment['sep_id'] = $row['sep_id'] === null ? null : (int)$row['sep_id'];
            $comment['mes_ref'] = $row['mes_ref'] === null ? null : (int)$row['mes_ref'];
            $comment['comment'] = $row['comment'];
            $comment['post'] = (int)$row['post'];
            $comment['date'] = $row['date'];
            $comment['reference'] = (int)$row['reference'];
            $comment['author'] = $row['author'];
            $comment['userpic'] = $row['userpic'];
            $comment['ratingup'] = (int)$row['ratingup'];
            $comment['ratingdown'] = (int)$row['ratingdown'];
            $comment['saved'] = (int)$row['saved'];
            $comment['attachments'] = $row['attachments'];
            $comment['currentuserrate'] = $row['currentuserrate'] === null ? null : (bool)$row['currentuserrate'];
            $comment['currentusersaved'] = (bool)$row['currentusersaved'];
            $comment['warning'] = $row['warning'];
            $this->data['comments'][] = $comment;
        }
        $this->result = true;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchBlockedTags() {
        $query = $this->conn->query("SELECT tags.tag FROM banned_tags LEFT JOIN tags ON banned_tags.tag = tags.id WHERE banned_tags.user = $this->user ORDER BY tags.tag ASC");
        if ($query->num_rows === 0) {
            $this->result = null;
            return;
        }
        while($row = $query->fetch_assoc()) {
            $this->data['blocked_tags'][] = $row['tag'];
        }
        $this->result = true;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchIgnoredUsers() {
        $query = $this->conn->query("SELECT users.username FROM users RIGHT JOIN ignored_users ON ignored_users.target_user = users.id WHERE ignored_users.user = $this->user ORDER BY users.username");
        if ($query->num_rows == 0) {
            $this->result = null;
            return;
        }
        while($row = $query->fetch_assoc()) {
            $this->data['ignored_users'][] = $row;
        }
        $this->result = true;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchSubscribedUsers() {
        $query = $this->conn->query("SELECT users.username, users.userpic FROM users RIGHT JOIN subscriptions ON subscriptions.target_user = users.id WHERE subscriptions.user = $this->user ORDER BY users.username");
        if ($query->num_rows == 0) {
            $this->result = null;
            return;
        }
        while($row = $query->fetch_assoc()) {
            $this->data['subscribed_users'][] = $row;
        }
        $this->result = true;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchUsers() {
        $query = $this->conn->query(
            "SELECT users.username, users.userpic, count(case postrates.sign when 1 then 1 else null end) - count(case postrates.sign when 0 then 1 else null end) AS rating FROM
                (SELECT users.username, users.userpic, posts.id AS post FROM users LEFT JOIN posts ON users.id = posts.user) AS users
            LEFT JOIN postrates ON users.post = postrates.post GROUP BY users.username ORDER BY rating DESC LIMIT 10");
        if($query->num_rows == 0) {
            $this->result = null;
            return;
        }
        while($row = $query->fetch_assoc()) {
            $user['username'] = $row['username'];
            $user['userpic'] = $row['userpic'];
            $user['rating'] = (int)$row['rating'];
            $this->data['users'][] = $user;
        }
        $this->result = true;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function fetchUser($form) {
        $stmt = $this->conn->prepare(
            "SELECT users.username, users.registration, users.userpic, users.description, users.rating, users.posts, users.currentuserignored, users.currentusersubscribed, users.subscribed, users.warning, users.banned, count(comments.id) AS comments FROM
                (SELECT count(case postrates.sign when 1 then 1 else null end) - count(case postrates.sign when 0 then 1 else null end) AS rating, count(distinct users.post) as posts, users.id, users.username, users.registration, users.userpic, users.description, users.currentuserignored, users.currentusersubscribed, users.subscribed, users.warning, users.banned FROM
                    (SELECT posts.id AS post, users.* FROM
                        (SELECT users.*, COUNT(subscriptions.id) AS subscribed FROM
                            (SELECT users.*, IF(subscriptions.target_user, 1, 0) AS currentusersubscribed FROM
                                (SELECT users.id, users.username, users.userpic, users.registration, users.description, users.warning, IF(users.status = 'banned', 1, 0) AS banned, IF(ignored_users.target_user, 1, 0) AS currentuserignored FROM users LEFT JOIN (SELECT target_user FROM ignored_users WHERE user = ?) AS ignored_users ON ignored_users.target_user = users.id  WHERE users.username = ?) AS users
                            LEFT JOIN (SELECT target_user FROM subscriptions WHERE user = ?) AS subscriptions ON subscriptions.target_user = users.id) AS users
                        LEFT JOIN subscriptions ON users.id = subscriptions.target_user) AS users
                    LEFT JOIN posts ON users.id = posts.user) AS users
                LEFT JOIN postrates ON users.post = postrates.post GROUP BY users.username) AS users
            LEFT JOIN comments ON users.id = comments.user");
        $stmt->bind_param('isi', $this->user, $form['username'], $this->user);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows == 0) {
            $stmt->close();
            $this->result = null;
            return;
        }
        $this->result = true;
        $row = $result->fetch_assoc();
        if (!$row['username']) {
            $this->result = null;
            $stmt->close();
            return;
        }
        $this->data['user']['username'] = $row['username'];
        $this->data['user']['registration'] = $row['registration'];
        $this->data['user']['userpic'] = $row['userpic'];
        $this->data['user']['description'] = $row['description'];
        $this->data['user']['rating'] = (int)$row['rating'];
        $this->data['user']['posts'] = (int)$row['posts'];
        $this->data['user']['subscribed'] = (int)$row['subscribed'];
        $this->data['user']['comments'] = (int)$row['comments'];
        $this->data['user']['currentusersubscribed'] = (bool)$row['currentusersubscribed'];
        $this->data['user']['currentuserignored'] = (bool)$row['currentuserignored'];
        $this->data['user']['banned'] = (bool)$row['banned'];
        $this->data['user']['warning'] = $row['warning'];
        $stmt->close();

    }
//--------------------------------------------------------------------------------------------------------------------------------
    private function checkNewMessages() {
        $query = $this->conn->query("SELECT COUNT(id) AS new FROM messages WHERE recipient = $this->user AND isread IS NULL");
        $row = $query->fetch_assoc();
        $this->result = true;
        $this->data['new_messages'] = (int)$row['new'];
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public function response() {
        $result['result'] = $this->result;
        $result['data'] = $this->data;
        return $result;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public function __destruct() {
        if ($this->conn) {
            $this->conn->close();
        }
    }
}
