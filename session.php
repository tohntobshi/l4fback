<?php
include_once 'db-conn.php';
include_once 'json-handler.php';
include_once 'common-functions.php';
include_once 'config.php';

class Session extends DBConn {
    private $conn;
    private $session_id;
    private $device;
    public $user;
    private $ip;
    private $user_agent;
    private $autologintry = false;
    public function __construct() {
        session_set_cookie_params(0, '/', '', HTTPS_COOKIE, true);
        session_start();
        $this->conn = $this->connect();
        $this->user = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
        $this->session_id = session_id();
        $this->ip = $this->conn->real_escape_string($_SERVER['REMOTE_ADDR']);
        $this->user_agent = $this->conn->real_escape_string($_SERVER['HTTP_USER_AGENT']);
        if (!preg_match("/^[a-z0-9]*$/u", $this->session_id)) {
            session_regenerate_id(true);
            $this->session_id = session_id();
        }
    }
    public function easySessionCheck() {
        if ($this->user === 0) {
            return;
        }
        if (!isset($_SESSION['ip']) || !isset($_SESSION['user_agent']) || !isset($_SESSION['last_updated'])) {
            $this->checkSession();
            return;
        }
        if ($_SESSION['ip'] != $this->ip || $_SESSION['user_agent'] != $this->user_agent || $_SESSION['last_updated'] < time() - 7*60) {
            $this->checkSession();
        }
    }
    public function checkSession() {
        $query = $this->conn->query("SELECT users.id AS user_id, users.username, users.userpic, users.status, users.scrolltype, users.numposts, users.show_terror, sessions.device, sessions.ip, sessions.user_agent  FROM users INNER JOIN sessions ON users.id = sessions.user WHERE sessions.session = '$this->session_id'");
        if ($query->num_rows == 0) {
            session_unset();
            $this->user = 0;
            $this->device = 0;
            $response['result'] = null;
            if (!$this->autologintry && isset($_COOKIE['autologin'])) {
                return $this->autoLogIn();
            }
            return $response;
        }
        $row = $query->fetch_assoc();
        $this->user = (int)$row['user_id'];
        $this->device = (int)$row['device'];
        if ($row['ip'] != $this->ip || $row['user_agent'] != $this->user_agent) {
            error_log("Auto log out. Prev ip: ".$row['ip'].". Current ip: ".$this->ip.". Prev user agent: ".$row['user_agent'].". Current user agent: ".$this->user_agent.". User id: ".$this->user);
            $this->deleteSessions();
            $response['result'] = null;
            return $response;
        }
        if (in_array($row['status'], ['banned', 'new'])) {
            $this->deleteSessions();
            $response['result'] = null;
            return $response;
        }
        $this->conn->query("UPDATE sessions SET last_updated = NOW() WHERE session = '$this->session_id'");
        $_SESSION['user_id'] = (int)$row['user_id'];
        $_SESSION['scrolltype'] = $row['scrolltype'];
        $_SESSION['numposts'] = (int)$row['numposts'];
        $_SESSION['show_terror'] = (bool)$row['show_terror'];
        $_SESSION['status'] = $row['status'];
        $_SESSION['last_updated'] = time();
        $_SESSION['ip'] = $this->ip;
        $_SESSION['user_agent'] = $this->user_agent;

        $response['result'] = true;
        $response['data']['session']['username'] = $row['username'];
        $response['data']['session']['userpic'] = $row['userpic'];
        $response['data']['session']['status'] = $row['status'];
        $response['data']['session']['scrolltype'] = $row['scrolltype'];
        $response['data']['session']['numposts'] = (int)$row['numposts'];
        $response['data']['session']['show_terror'] = (bool)$row['show_terror'];
        return $response;
    }
    private function autoLogIn() {
        $this->autologintry = true;
        $auto_login = $_COOKIE['autologin'];
        $hashed_auto_login = password_hash($auto_login, PASSWORD_BCRYPT, array('salt' => AUTOLOGIN_SALT));
        session_regenerate_id(true);
        $this->session_id = session_id();
        $this->conn->query("INSERT INTO sessions (user, session, device, last_updated, ip, user_agent) SELECT (SELECT user FROM autologin WHERE auto_login = '$hashed_auto_login') AS user, '$this->session_id' AS session, ((SELECT IFNULL(MAX(device), 0) FROM sessions WHERE user = (SELECT user FROM autologin WHERE auto_login = '$hashed_auto_login')) + 1 ) AS device, NOW() AS last_updated, '$this->ip' AS ip, '$this->user_agent' AS user_agent FROM dual WHERE (SELECT user FROM autologin WHERE auto_login = '$hashed_auto_login') IS NOT NULL");
        $response = $this->checkSession();
        if ($this->user != 0) {
        $new_auto_login = CommonFunctions::generateRandomString(32);
        setcookie('autologin', $new_auto_login, time()+60*60*24*30, '/', '', HTTPS_COOKIE, true);
            $new_hashed_auto_login = password_hash($new_auto_login, PASSWORD_BCRYPT, array('salt' => AUTOLOGIN_SALT));
            $this->conn->query("UPDATE autologin SET auto_login = '$new_hashed_auto_login', last_updated = NOW() WHERE auto_login = '$hashed_auto_login'");
        } else {
            setcookie("autologin", null, time() - 3600);
        }
        return $response;
    }
    public function logOutEverywhere() {
        if ($this->user === 0) {
            $response['result'] = null;
            return $response;
        }
        $this->deleteSessions();
        $response['result'] = true;
        return $response;
    }

    private function deleteSessions() {
        $this->conn->query("DELETE FROM autologin WHERE user = $this->user");
        $this->conn->query("DELETE FROM sessions WHERE user = $this->user");
        session_unset();
        $this->user = 0;
        $this->device = 0;
        setcookie("autologin", null, time() - 3600);
    }


    public function logInVk($form) {
        if ($this->user != 0) {
            $this->logOut();
        }
        session_regenerate_id(true);
        $this->session_id = session_id();
        if (!isset($form['expire']) || !isset($form['mid']) || !isset($form['secret']) || !isset($form['sid']) || !isset($form['sig']) || !isset($form['remember'])) {
            $response['result'] = null;
            return $response;
        }
        $check = "expire=".$form['expire']."mid=".$form['mid']."secret=".$form['secret']."sid=".$form['sid'].VK_SECRET;
        if ($form['sig'] !== md5($check)) {
            $response['result'] = null;
            return $response;
        }
        $stmt = $this->conn->prepare("SELECT id, status FROM users WHERE tp_id=? AND acc_type = 'vk'");
        $stmt->bind_param('s', $form['mid']);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows === 0) {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "user_not_exist";
            return $response;
        }
        $row = $result->fetch_assoc();
        if ($row['status'] === 'banned') {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "banned";
            return $response;
        }
        $stmt->close();
        $user_id = (int)$row['id'];
        $this->conn->query("INSERT INTO sessions (user, session, device, last_updated, ip, user_agent) SELECT $user_id AS user, '$this->session_id' AS session, ((SELECT IFNULL(MAX(device), 0) FROM sessions WHERE user = $user_id) + 1 ) AS device, NOW() AS last_updated, '$this->ip' AS ip, '$this->user_agent' AS user_agent FROM dual");
        if ($form['remember']) {
            $auto_login = CommonFunctions::generateRandomString(32);
            setcookie('autologin', $auto_login, time()+60*60*24*30, '/', '', HTTPS_COOKIE, true);
            $hashed_auto_login = password_hash($auto_login, PASSWORD_BCRYPT, array('salt' => AUTOLOGIN_SALT));
            $this->conn->query("INSERT INTO autologin (user, auto_login, last_updated) VALUES ($user_id, '$hashed_auto_login', NOW())");
        }
        return $this->checkSession();
    }
    public function logInGoogle($form) {
        if ($this->user != 0) {
            $this->logOut();
        }
        session_regenerate_id(true);
        $this->session_id = session_id();
        if (!isset($form['id_token']) || !isset($form['remember'])) {
            $response['result'] = null;
            return $response;
        }
        $google_id = CommonFunctions::googleAuth($form['id_token']);
        if (!$google_id) {
            $this->result = null;
            return;
        }
        $stmt = $this->conn->prepare("SELECT id, status FROM users WHERE tp_id=? AND acc_type = 'google'");
        $stmt->bind_param('s', $google_id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows === 0) {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "user_not_exist";
            return $response;
        }
        $row = $result->fetch_assoc();
        if ($row['status'] === 'banned') {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "banned";
            return $response;
        }
        $stmt->close();
        $user_id = (int)$row['id'];
        $this->conn->query("INSERT INTO sessions (user, session, device, last_updated, ip, user_agent) SELECT $user_id AS user, '$this->session_id' AS session, ((SELECT IFNULL(MAX(device), 0) FROM sessions WHERE user = $user_id) + 1 ) AS device, NOW() AS last_updated, '$this->ip' AS ip, '$this->user_agent' AS user_agent FROM dual");
        if ($form['remember']) {
            $auto_login = CommonFunctions::generateRandomString(32);
            setcookie('autologin', $auto_login, time()+60*60*24*30, '/', '', HTTPS_COOKIE, true);
            $hashed_auto_login = password_hash($auto_login, PASSWORD_BCRYPT, array('salt' => AUTOLOGIN_SALT));
            $this->conn->query("INSERT INTO autologin (user, auto_login, last_updated) VALUES ($user_id, '$hashed_auto_login', NOW())");
        }
        return $this->checkSession();
    }
    public function logIn($form) {
        if ($this->user != 0) {
            $this->logOut();
        }
        session_regenerate_id(true);
        $this->session_id = session_id();
        if (!isset($form['username']) || !isset($form['password']) || !isset($form['captcha']) || !isset($form['remember'])) {
            $response['result'] = null;
            return $response;
        }
        if (mb_strlen($form['username']) > 64) {
            $response['result'] = null;
            return $response;
        }
        if (!CommonFunctions::checkCaptcha($form['captcha'])) {
            $response['result'] = null;
            $response['error'] = "bad_captcha";
            return $response;
        }
        $stmt = $this->conn->prepare("SELECT id, password, status FROM users WHERE (username=? OR email=?) AND acc_type = 'normal'");
        $stmt->bind_param('ss', $form['username'], $form['username']);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows === 0) {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "user_not_exist";
            return $response;
        }
        $row = $result->fetch_assoc();
        if ($row['status'] === 'banned') {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "banned";
            return $response;
        }
        if ($row['status'] === 'new') {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "new";
            return $response;
        }
        if(!password_verify($form['password'], $row['password'])) {
            $stmt->close();
            $response['result'] = null;
            $response['error'] = "wrong_password";
            return $response;
        }
        $stmt->close();
        $user_id = (int)$row['id'];
        $this->conn->query("INSERT INTO sessions (user, session, device, last_updated, ip, user_agent) SELECT $user_id AS user, '$this->session_id' AS session, ((SELECT IFNULL(MAX(device), 0) FROM sessions WHERE user = $user_id) + 1 ) AS device, NOW() AS last_updated, '$this->ip' AS ip, '$this->user_agent' AS user_agent FROM dual");
        if ($form['remember']) {
            $auto_login = CommonFunctions::generateRandomString(32);
            setcookie('autologin', $auto_login, time()+60*60*24*30, '/', '', HTTPS_COOKIE, true);
            $hashed_auto_login = password_hash($auto_login, PASSWORD_BCRYPT, array('salt' => AUTOLOGIN_SALT));
            $this->conn->query("INSERT INTO autologin (user, auto_login, last_updated) VALUES ($user_id, '$hashed_auto_login', NOW())");
        }
        return $this->checkSession();
    }

    public function logOut() {
        if ($this->user === 0) {
            $response['result'] = null;
            return $response;
        }
        session_unset();
        $query = $this->conn->query("DELETE FROM sessions WHERE user = $this->user AND device = $this->device");
        if (isset($_COOKIE['autologin'])) {
            $auto_login = $_COOKIE['autologin'];
            $hashed_auto_login = password_hash($auto_login, PASSWORD_BCRYPT, array('salt' => AUTOLOGIN_SALT));
            $query = $this->conn->query("DELETE FROM autologin WHERE auto_login = '$hashed_auto_login'");
            setcookie("autologin", null, time() - 3600);
        }
        $response['result'] = true;
        return $response;
    }
    public function fetchActiveSessions() {
        if ($this->user == 0) {
            $response['result'] = null;
            return $response;
        }
        $query = $this->conn->query("SELECT COUNT(DISTINCT device) AS active_sessions FROM sessions WHERE user = $this->user");
        $row = $query->fetch_assoc();
        $response['data']['active_sessions'] = (int)$row['active_sessions'];
        $response['result'] = true;
        return $response;
    }
    public function __destruct() {
        $this->conn->close();
    }
}
