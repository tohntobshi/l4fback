<?php
include_once 'db-conn.php';

class ContentCreator extends DBConn {
    private $conn;
    private $user;
    public $result;
    public function __construct($form) {
        if (!isset($form['query'])) {
            $this->result = null;
            return;
        }
        if (!isset($_SESSION['user_id'])) {
            $this->result = null;
            return;
        }
        $this->user = $_SESSION['user_id'];
        $this->conn = $this->connect();
        switch ($form['query']) {
            case 'create_post':
                if (!isset($form['title']) || !isset($form['attachments']) || !isset($form['hr']) || !isset($form['auth_only']) || !isset($form['terror'])) {
                    $this->result = null;
                    return;
                }
                $this->createPost($form);
                break;
            case 'create_comment':
                if (!isset($form['comment']) || !isset($form['post_id']) || !isset($form['reference'])) {
                    $this->result = null;
                    return;
                }
                $this->createComment($form);
                break;
            default:
                $this->result = null;
                return;
            }
    }

    private function createPost($form) {
        $stmt = $this->conn->prepare("INSERT INTO posts (title, description, user, main_att, hr, terror, auth_only, date) VALUES (?,?,?,?,?,?,?, NOW())");
        $main_att = isset($form['main_att']) ? (int)$form['main_att'] < count($form['attachments']) ? (int)$form['main_att'] : 0 : 0;
        $description = (isset($form['description'])) ? mb_substr($form['description'], 0, 5000) : "";
        $title = mb_substr($form['title'], 0, 128);
        $hr = (bool)$form['hr'];
        $terror = (bool)$form['terror'];
        $auth_only = (bool)$form['auth_only'];
        $stmt->bind_param('ssiiiii', $title, $description, $this->user, $main_att, $hr, $terror, $auth_only);
        $stmt->execute();
        $post_id = $this->conn->insert_id;
        $this->result = true;
        $stmt->close();
        if (isset($form['tags'])) {
            $stmt2 = $this->conn->prepare("INSERT INTO tags (tag) SELECT ? AS tag FROM dual WHERE NOT EXISTS(SELECT tag FROM tags WHERE tag = ?)");
            $stmt2->bind_param('ss', $tag, $tag);
            $stmt3 = $this->conn->prepare("INSERT INTO tagmap (tag, post) VALUES ((SELECT id FROM tags WHERE tag = ?), ?)");
            $stmt3->bind_param('si', $tag, $post_id);
            $i = 0;
            foreach ($form['tags'] as $value) {
                if($i == 10) {
                    break;
                }
                $tag = (string)$value;
                if (mb_strlen($tag) > 32 || !preg_match("/^[a-z0-9а-я\- ]*$/u", $tag)) {
                    continue;
                }
                $stmt2->execute();
                $stmt3->execute();
                $i++;
            }
            $stmt2->close();
            $stmt3->close();
        }
        $this->addAttachments($form['attachments'], "post", $post_id, 10);
    }

    private function createComment($form) {
        $stmt = $this->conn->prepare("INSERT INTO comments (user, post, reference, comment, date) VALUES (?,?,?,?, NOW())");
        $comment = mb_substr($form['comment'], 0, 5000);
        $stmt->bind_param('iiis', $this->user, $form['post_id'], $form['reference'], $comment);
        $stmt->execute();
        $comment_id = $this->conn->insert_id;
        $this->result = true;
        $stmt->close();
        if (isset($form['attachments'])) {
            $this->addAttachments($form['attachments'], "comment", $comment_id, 5);
        }
        if (isset($form['recipients'])) {
            $stmt2 = $this->conn->prepare("INSERT INTO messages (recipient, comment) SELECT (SELECT id FROM users WHERE username = ?) AS recipient, ? AS comment FROM dual WHERE NOT EXISTS (SELECT id FROM ignored_users WHERE user = (SELECT id FROM users WHERE username = ?) AND target_user = ?)");
            $stmt2->bind_param('sisi', $recipient, $comment_id, $recipient, $this->user);
            foreach ($form['recipients'] as $val) {
                $recipient = $val;
                $stmt2->execute();
            }
            $stmt2->close();
        }
        if ($form['reference'] != 0) {
            $stmt3 = $this->conn->prepare("INSERT INTO messages (recipient, comment, reference) SELECT (SELECT user FROM comments WHERE id = ?) AS recipient, ? AS comment, ? AS reference FROM dual WHERE NOT EXISTS (SELECT id FROM ignored_users WHERE user = (SELECT user FROM comments WHERE id = ?) AND target_user = ?) AND (SELECT user FROM comments WHERE id = ?) != ?");
            $stmt3->bind_param('iiiiiii', $form['reference'], $comment_id, $form['reference'], $form['reference'], $this->user, $form['reference'], $this->user);
            $stmt3->execute();
            $stmt3->close();
        }
    }

    private function addAttachments($attachments, $type, $id, $limit) {
        $stmt = $this->conn->prepare("INSERT INTO attachments (src, type) SELECT ? AS src, ? AS type FROM dual WHERE NOT EXISTS (SELECT id FROM attachments WHERE src = ? AND type = ?)");
        $stmt->bind_param('ssss', $src, $att_type, $src, $att_type);
        $stmt2 = $this->conn->prepare("INSERT INTO attachment_map (".$type.", attachment) VALUES (?, (SELECT id FROM attachments WHERE src = ? AND type = ? AND additional IS NULL))");
        $stmt2->bind_param('iss', $id, $src, $att_type);
        $i = 0;
        foreach ($attachments as $attachment) {
            if($i == $limit) {
                break;
            }
            if (!isset($attachment['type']) || !isset($attachment['src'])) {
                break;
            }
            $att_type = $attachment['type'];
            $src = $attachment['src'];
            if(!preg_match("/^[a-zA-Z0-9_\-\.\/]*$/u", $src) || mb_strlen($att_type) > 10 || mb_strlen($src) > 128 || !$att_type || !$src) {
                continue;
            }
            if (!in_array($att_type, ['jpg', 'gif'])) {
                $stmt->execute();
            }
            $stmt2->execute();
            $i++;
        }
        $stmt->close();
        $stmt2->close();
    }

    public function __destruct() {
        if ($this->conn) {
            $this->conn->close();
        }
    }
}
