<?php
include_once 'db-conn.php';

class Admin extends DBConn {
    private $conn;
    private $result;
    private $data;
    public function __construct($form) {
        if (!isset($_SESSION['status'])) {
            $this->result = null;
            return;
        }
        if ($_SESSION['status'] != 'admin') {
            $this->result = null;
            return;
        }
        if (!isset($form['query'])) {
            $this->result = null;
            return;
        }
        $this->conn = $this->connect();
        switch ($form['query']) {
            case 'ban':
                if (!isset($form['username'])) {
                    $this->result = null;
                    return;
                }
                $this->banUser($form);
                break;
            case 'unban':
                if (!isset($form['username'])) {
                    $this->result = null;
                    return;
                }
                $this->unbanUser($form);
                break;
            case 'warn_user':
                if (!isset($form['username']) || !isset($form['warning'])) {
                    $this->result = null;
                    return;
                }
                $this->warnUser($form);
                break;
            case 'warn_comment':
                if (!isset($form['id']) || !isset($form['warning'])) {
                    $this->result = null;
                    return;
                }
                $this->warnComment($form);
                break;
            case 'edit':
                if (!isset($form['text']) || !isset($form['type']) || !isset($form['id'])) {
                    $this->result = null;
                    return;
                }
                $this->edit($form);
                break;
            case 'delete_attachment':
                if (!isset($form['id']) || !isset($form['content_type']) || !isset($form['src']) || !isset($form['type'])) {
                    $this->result = null;
                    return;
                }
                $this->deleteAttachment($form);
                break;
            case 'delete_post':
                if (!isset($form['id']) || !isset($form['all_comments'])) {
                    $this->result = null;
                    return;
                }
                $this->deletePost($form);
                break;
            case 'delete_comment':
                if (!isset($form['id'])) {
                    $this->result = null;
                    return;
                }
                $this->deleteComment($form);
                break;
            case 'delete_all':
                if (!isset($form['username']) || !isset($form['date'])) {
                    $this->result = null;
                    return;
                }
                $this->deleteAllUsersContent($form);
                break;
            case 'change_post_attributes':
                if (!isset($form['id']) || !isset($form['hr']) || !isset($form['terror']) || !isset($form['auth_only'])) {
                    $this->result = null;
                    return;
                }
                $this->changePostAttributes($form);
                break;
            case 'fetch_complaints':
                $this->fetchComplaints();
                break;
            case 'delete_complaint':
                if (!isset($form['type']) || !isset($form['id'])) {
                    $this->result = null;
                    return;
                }
                $this->deleteComplaint($form);
                break;
            case 'delete_user_complaints':
                if (!isset($form['username'])) {
                    $this->result = null;
                    return;
                }
                $this->deleteUserComplaints($form);
                break;
            default:
                $this->result = null;
                return;
        }
    }

    private function banUser($form) {
        $username = $this->conn->real_escape_string($form['username']);
        $this->conn->query("UPDATE users SET status = 'banned' WHERE username = '$username' AND status != 'admin'");
        if ($this->conn->affected_rows < 1) {
            $this->result = null;
            return;
        }
        $this->result = true;
    }
    private function unbanUser($form) {
        $username = $this->conn->real_escape_string($form['username']);
        $this->conn->query("UPDATE users SET status = 'normal' WHERE username = '$username' AND status = 'banned'");
        if ($this->conn->affected_rows < 1) {
            $this->result = null;
            return;
        }
        $this->result = true;
    }
    private function warnUser($form) {
        $username = $this->conn->real_escape_string($form['username']);
        $warning = $this->conn->real_escape_string($form['warning']);
        $warning = mb_substr($warning, 0, 256);
        $this->conn->query("UPDATE users SET warning = '$warning' WHERE username = '$username' AND status != 'admin'");
        $this->result = true;
        if (!$warning) {
            return;
        }
        $this->conn->query("INSERT INTO comments (comment, date, user) VALUES ('$warning', NOW(), (SELECT id FROM users WHERE username = 'moderator'))");
        $comment_id = $this->conn->insert_id;
        $this->conn->query("INSERT INTO messages (recipient, comment) VALUES ((SELECT id FROM users WHERE username = '$username'), $comment_id)");
    }
    private function warnComment($form) {
        $comment_id = (int)$form['id'];
        $warning = $this->conn->real_escape_string($form['warning']);
        $warning = mb_substr($warning, 0, 256);
        $this->conn->query("UPDATE comments SET warning = '$warning' WHERE id = $comment_id");
        $this->result = true;
        if (!$warning) {
            return;
        }
        $this->conn->query("INSERT INTO messages (recipient, comment) VALUES ((SELECT user FROM comments WHERE id = $comment_id), $comment_id)");
    }
    private function edit($form) {
        $text = $this->conn->real_escape_string($form['text']);
        $type = $form['type'];
        $id = (int)$form['id'];
        switch ($type) {
            case 'title':
                $text = mb_substr($text, 0, 128);
                break;
            case 'description':
            case 'comment':
                $text = mb_substr($text, 0, 5000);
                break;
            default:
                $this->result = null;
                return;
        }
        $table = $type == 'comment' ? 'comments' : 'posts';
        $this->conn->query("UPDATE $table SET $type = '$text' WHERE id = $id");
        if ($this->conn->affected_rows < 1) {
            $this->result = null;
            return;
        }
        $this->result = true;
    }

    private function findNestedComments($ids) { // there may be one or more ids separated by comma, for example $ids='1' or $ids = '1,2,3,4'
        if ($ids == null) {
            return null;
        }
        $this->conn->query(
            "BEGIN NOT ATOMIC
	            SET @curid = '$ids';
                SET @allid = '$ids';
                REPEAT
                    SET @curid = (SELECT GROUP_CONCAT(id SEPARATOR ',') FROM comments WHERE FIND_IN_SET(reference, @curid));
                    IF @curid IS NOT NULL
                    THEN
                        SET @allid = CONCAT(@allid,',',@curid);
                    END IF;
	           UNTIL @curid IS NULL END REPEAT;
	        END"
        );
        $i = 0;
        $query = $this->conn->query("SELECT @allid AS comments");
        $row = $query->fetch_assoc();
        return $row['comments'];
    }
    private function deleteAttachment($form) {
        $id = (int)$form['id'];
        $content_type = $form['content_type'];
        $type = $this->conn->real_escape_string($form['type']);
        $src = $this->conn->real_escape_string($form['src']);
        if (!in_array($content_type, ['post', 'comment'])) {
            $this->result = null;
            return;
        }
        if($content_type === 'post') {
            $this->conn->query("UPDATE posts SET main_att = 0 WHERE id = $id");
        }
        $this->conn->query("DELETE FROM attachment_map WHERE $content_type = $id AND attachment = (SELECT id FROM attachments WHERE src = '$src' AND type = '$type' AND additional IS NULL)");
        $this->result = true;
    }
    private function deletePost($form) {
        $all = (bool)$form['all_comments'];
        $id = (int)$form['id'];
        if ($all) { // all comments
            $sql = "SELECT GROUP_CONCAT(id SEPARATOR ',') AS comment_ids FROM comments WHERE post = $id";
        } else { // all comments except saved to favorite at least by one user
            $sql = "SELECT GROUP_CONCAT(comments.id SEPARATOR ',') AS comment_ids FROM comments LEFT JOIN favorite_comments ON comments.id = favorite_comments.comment WHERE post = $id AND favorite_comments.comment IS NULL";
        }
        $query2 = $this->conn->query($sql);
        if ($query2->num_rows > 0) {
            $row2 = $query2->fetch_assoc();
            $comment_ids = $row2['comment_ids'];
            $this->deleteComments($comment_ids);
        }
        $this->conn->query("DELETE FROM posts WHERE id = $id");
        $this->conn->query("DELETE FROM favorite_posts WHERE post = $id");
        $this->conn->query("DELETE FROM postrates WHERE post = $id");
        $this->conn->query("DELETE FROM attachment_map WHERE post = $id");
        $this->conn->query("DELETE FROM post_complaints WHERE post = $id");
        $this->conn->query("DELETE FROM tagmap WHERE post = $id");
        $this->result = true;
    }
    private function deleteComment($form) {
        $id = (int)$form['id'];
        $ids = $this->findNestedComments($id);
        $this->deleteComments($ids);
        $this->result = true;
    }
    private function deleteComments($ids) { // list of ids separated by comma
        if ($ids == null) {
            return;
        }
        $this->conn->query("DELETE FROM comments WHERE FIND_IN_SET(id, '$ids')");
        $this->conn->query("DELETE FROM commentrates WHERE FIND_IN_SET(comment, '$ids')");
        $this->conn->query("DELETE FROM attachment_map WHERE FIND_IN_SET(comment, '$ids')");
        $this->conn->query("DELETE FROM comment_complaints WHERE FIND_IN_SET(comment, '$ids')");
        $this->conn->query("DELETE FROM favorite_comments WHERE FIND_IN_SET(comment, '$ids')");
        $this->conn->query("DELETE FROM messages WHERE FIND_IN_SET(comment, '$ids')");
    }
    private function deleteAllUsersContent($form) {
        $username = $this->conn->real_escape_string($form['username']);
        $date = (string)$form['date'];
        if (!preg_match('/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', $date)) {
            $this->result = null;
            return;
        }
        // all comments to the users's posts
        $query = $this->conn->query("SELECT GROUP_CONCAT(comments.id SEPARATOR ',') AS comment_ids FROM comments INNER JOIN posts ON posts.id = comments.post WHERE posts.user = (SELECT id FROM users WHERE username = '$username') AND posts.date >= '$date'");
        $row = $query->fetch_assoc();
        $this->deleteComments($row['comment_ids']);
        // all comments of the user
        $query3 = $this->conn->query("SELECT GROUP_CONCAT(id SEPARATOR ',') AS comment_ids FROM comments WHERE user = (SELECT id FROM users WHERE username = '$username') and date >= '$date'");
        $row3 = $query3->fetch_assoc();
        // all user's comments and nested comments
        $nested = $this->findNestedComments($row3['comment_ids']);
        $this->deleteComments($nested);
        $this->conn->query("DELETE favorite_posts FROM favorite_posts INNER JOIN posts ON posts.id = favorite_posts.post WHERE posts.user = (SELECT id FROM users WHERE username = '$username') AND posts.date >= '$date'");
        $this->conn->query("DELETE postrates FROM postrates INNER JOIN posts ON posts.id = postrates.post WHERE posts.user = (SELECT id FROM users WHERE username = '$username') AND posts.date >= '$date'");
        $this->conn->query("DELETE attachment_map FROM attachment_map INNER JOIN posts ON posts.id = attachment_map.post WHERE posts.user = (SELECT id FROM users WHERE username = '$username') AND posts.date >= '$date'");
        $this->conn->query("DELETE post_complaints FROM post_complaints INNER JOIN posts ON posts.id = post_complaints.post WHERE posts.user = (SELECT id FROM users WHERE username = '$username') AND posts.date >= '$date'");
        $this->conn->query("DELETE tagmap FROM tagmap INNER JOIN posts ON posts.id = tagmap.post WHERE posts.user = (SELECT id FROM users WHERE username = '$username') AND posts.date >= '$date'");
        $this->conn->query("DELETE FROM posts WHERE user = (SELECT id FROM users WHERE username = '$username') AND date >= '$date'");
        $this->result = true;
    }
    private function changePostAttributes($form) {
        $id = (int)$form['id'];
        $hr = (int)(bool)$form['hr'];
        $terror = (int)(bool)$form['terror'];
        $auth_only = (int)(bool)$form['auth_only'];
        $this->conn->query("UPDATE posts SET hr = $hr, terror = $terror, auth_only = $auth_only WHERE id = $id");
        $this->result = true;
    }
    private function fetchComplaints() {
        $query = $this->conn->query(
            "SELECT IFNULL(complaints.c_title, complaints.title) AS title, IFNULL(complaints.c_post, complaints.post) AS post, complaints.comment, complaints.reasons, complaints.users FROM
                (SELECT comments.comment AS c_title, comments.post AS c_post, complaints.* FROM
                    (SELECT posts.title, complaints.* FROM
                        (SELECT MIN(pcomplaints.date) AS date, MIN(pcomplaints.id) AS id, pcomplaints.post, NULL AS comment, GROUP_CONCAT(DISTINCT pcomplaints.reason SEPARATOR ',') AS reasons, GROUP_CONCAT(DISTINCT pcomplaints.username SEPARATOR ',') AS users FROM
                            (SELECT post_complaints.id, post_complaints.post, post_complaints.reason, post_complaints.date, users.username FROM post_complaints
                            LEFT JOIN users ON post_complaints.user = users.id) AS pcomplaints GROUP BY pcomplaints.post
                        UNION
                        SELECT MIN(ccomplaints.date) AS date, MIN(ccomplaints.id) AS id, NULL AS post, ccomplaints.comment, GROUP_CONCAT(DISTINCT ccomplaints.reason SEPARATOR ',') AS reasons, GROUP_CONCAT(DISTINCT ccomplaints.username SEPARATOR ',') AS users FROM
                            (SELECT comment_complaints.id, comment_complaints.comment, comment_complaints.reason, comment_complaints.date, users.username FROM comment_complaints
                            LEFT JOIN users ON comment_complaints.user = users.id) AS ccomplaints GROUP BY ccomplaints.comment
                        ORDER BY date, id LIMIT 20) AS complaints
                    LEFT JOIN posts ON posts.id = complaints.post) AS complaints
                LEFT JOIN comments ON comments.id = complaints.comment) AS complaints"
        );
        if ($query->num_rows === 0) {
            $this->result = null;
            return;
        }
        while ($row = $query->fetch_assoc()) {
            $complaint['title'] = $row['title'];
            $complaint['reasons'] = $row['reasons'];
            $complaint['users'] = $row['users'];
            $complaint['post'] = (int)$row['post'];
            $complaint['comment'] = $row['comment'] === null ? null : (int)$row['comment'];
            $this->data['complaints'][] = $complaint;
        }
        $this->result = true;
    }
    private function deleteComplaint($form) {
        $type = $form['type'];
        $id = (int)$form['id'];
        if (!in_array($type, ['post', 'comment'])) {
            $this->result = null;
            return;
        }
        $this->conn->query("DELETE FROM ".$type."_complaints WHERE $type = $id");
        $this->result = true;
    }
    private function deleteUserComplaints($form) {
        $username = $this->conn->real_escape_string($form['username']);
        $this->conn->query("DELETE FROM post_complaints WHERE user = (SELECT id FROM users WHERE username = '$username')");
        $this->conn->query("DELETE FROM comment_complaints WHERE user = (SELECT id FROM users WHERE username = '$username')");
        $this->result = true;
    }
    public function response() {
        $response['result'] = $this->result;
        $response['data'] = $this->data;
        return $response;
    }
    public function __destruct() {
        if ($this->conn) {
            $this->conn->close();
        }
    }
}
