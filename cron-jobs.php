<?php
include_once 'db-conn.php';
include_once 'config.php';

class CronJobs extends DBConn {
    private $conn;
    public function __construct() {
        $this->conn = $this->connect();
    }
    public function deleteObsoleteSessions() {
        $this->conn->query("DELETE FROM sessions WHERE last_updated < timestampadd(minute, -10, NOW())");
    }
    public function deleteObsoleteAutologins() {
        $this->conn->query("DELETE FROM autologin WHERE last_updated < timestampadd(month, -1, NOW())");
    }
    public function deleteUnusedFiles() {
        $query = $this->conn->query("SELECT attachments.src, attachments.type FROM attachments LEFT JOIN attachment_map ON attachments.id = attachment_map.attachment WHERE attachment_map.attachment IS NULL AND attachments.type IN ('gif', 'jpg') AND attachments.additional IS NULL AND attachments.last_updated < timestampadd(day, -2, NOW())");
        if ($query->num_rows === 0) {
            return;
        }
        $stmt = $this->conn->prepare("DELETE FROM attachments WHERE src = ? AND type = ?");
        $stmt->bind_param("ss", $src, $type);
        while ($row = $query->fetch_assoc()) {
            $src = $row['src'];
            $type = $row['type'];
            $stmt->execute();
            unlink('/var/www/'.MY_SITE_NAME.'/public_html/useruploads/'.$src.'.jpg');
            if ($type === 'gif') {
                unlink('/var/www/'.MY_SITE_NAME.'/public_html/useruploads/'.$src.'.gif');
            }
        }
    }
    public function deleteRecreationPasswords() {
        $this->conn->query("UPDATE users SET recreation_password = NULL, recreation_date = NULL WHERE recreation_date < timestampadd(day, -1, NOW())");
    }
    public function __destruct() {
        $this->conn->close();
    }
}
