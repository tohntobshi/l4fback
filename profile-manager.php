<?php
include_once 'db-conn.php';

class ProfileManager extends DBConn {
    private $conn;
    private $user;
    private $result;
    private $error;
    public function __construct($form) {
        if (!isset($form['query'])) {
            $this->result = null;
            return;
        }
        if (!isset($_SESSION['user_id'])) {
            $this->result = null;
            return;
        }
        $this->conn = $this->connect();
        $this->user =  $_SESSION['user_id'];
        switch ($form['query']) {
            case 'apply_settings':
                if (!isset($form['scrolltype']) || !isset($form['numposts']) || !isset($form['show_terror'])) {
                    $this->result = null;
                    return;
                }
                $this->applySettings($form);
                break;
            case 'change_description':
                if (!isset($form['description'])) {
                    $this->result = null;
                    return;
                }
                $this->changeDescription($form);
                break;
            case 'complain':
                if (!isset($form['id']) || !isset($form['type']) || !isset($form['reason'])) {
                    $this->result = null;
                    return;
                }
                $this->complain($form);
                break;
            case 'delete_message':
                if (!isset($form['message'])) {
                    $this->result = null;
                    return;
                }
                $this->deleteMessage($form);
                break;
            case 'reset_messages':
                $this->resetMessages();
                break;
            case 'change_password':
                if (!isset($form['old_password']) || !isset($form['new_password'])) {
                    $this->result = null;
                    return;
                }
                $this->changePassword($form);
                break;
            default:
                $this->result = null;
                return;
        }

    }
    private function applySettings($form) {
        $numposts = (int)$form['numposts'];
        $scrolltype = $form['scrolltype'];
        $show_terror = (bool)$form['show_terror'];
        if ($numposts < 10 || $numposts > 30 || !in_array($scrolltype, ['pagination', 'infinite'])) {
            $this->result = null;
            return;
        }
        $sh_tr = (int)$show_terror;
        $this->conn->query("UPDATE users SET numposts = $numposts, scrolltype = '$scrolltype', show_terror = $sh_tr WHERE id = $this->user");
        $_SESSION['numposts'] = $numposts;
        $_SESSION['scrolltype'] = $scrolltype;
        $_SESSION['show_terror'] = $show_terror;
        $this->result = true;
    }
    private function changeDescription($form) {
        $description = mb_substr($form['description'], 0, 256);
        $stmt = $this->conn->prepare("UPDATE users SET description = ? WHERE id = ?");
        $stmt->bind_param('si', $description, $this->user);
        $stmt->execute();
        $stmt->close();
        $this->result = true;
    }
    private function resetMessages() {
        $this->conn->query("UPDATE messages SET isread = 1 WHERE recipient = $this->user");
        $this->result = true;
    }
    private function deleteMessage($form) {
        $mess_id = (int)$form['message'];
        $this->conn->query("DELETE FROM messages WHERE recipient = $this->user AND id = $mess_id");
        $this->result = true;
    }
    private function complain($form) {
        if (!in_array($form['type'], ['post', 'comment']) || !in_array($form['reason'], ['offence', 'cp', 'spam', 'extremism', 'flood', 'other'])) {
            $this->result = null;
            return;
        }
        $stmt = $this->conn->prepare("INSERT INTO ".$form['type']."_complaints (".$form['type'].", reason, user, date) VALUES (?, ?, ?, NOW())");
        $stmt->bind_param('isi', $form['id'], $form['reason'], $this->user);
        $stmt->execute();
        $stmt->close();
        $this->result = true;
    }
    private function changePassword($form) {
        $password = (string)$form['new_password'];
        if (strlen($password) < 6 || strlen($password) > 32) {
            $this->error = "invalid_new_password";
            $this->result = null;
            return;
        }
        $query = $this->conn->query("SELECT password FROM users WHERE id = $this->user");
        $row = $query->fetch_assoc();
        if(!password_verify($form['old_password'], $row['password'])) {
            $this->result = null;
            $this->error = "wrong_old_password";
            return;
        }
        $hashedPwd = password_hash($form['new_password'], PASSWORD_DEFAULT);
        $stmt = $this->conn->prepare("UPDATE users SET password = ? WHERE id = ?");
        $stmt->bind_param('si', $hashedPwd, $this->user);
        $stmt->execute();
        $stmt->close();
        $this->result = true;
    }
    public function response() {
        $response['result'] = $this->result;
        $response['error'] = $this->error;
        return $response;
    }

    public function __destruct() {
        if ($this->conn) {
            $this->conn->close();
        }
    }
}
